# Welcome to MYO Camera, an Android camera application that works with MYO armband #



 **System Architecture**

    a.	Software Modules: MYO android SDK
    b.	Hardware Components: Screen, storage, camera, Bluetooth, MYO armband
    c.	User interface: Same as integrated camera application on most android phones
    d.	Interfaces to external systems: Hardware device MYO armband


**Hardware, Software and System Requirements**

    a.	Hardware Requirements (Minimum requirements)

      1.  Screen display – Above 480 X 800 pixel       
      2.  Disk storage – Minimum of 1GB
      3.  Camera – No minimum requirements
      4.  Bluetooth 4.0 or higher
      5.  MYO armband

    b.	Software Requirements

      1.  MYO Android SDK 0.10.0

    c.	System Requirements

      1.  Android 5.0.0 (Lollipop) or higher


**External Interfaces**

    -  Myo armband uses Bluetooth Smart connection to pair with Android devices. 
      Each application controls the pairing and connection to the Myo armband within the application (How do I use, para.1).

# **Software Design** #

   ***Class Diagram***

![Class Diagram.PNG](https://bitbucket.org/repo/6kjaBb/images/2739290367-Class%20Diagram.PNG)

   ***Class Specification***

    -   For UML notations + means ‘public’, so it can be accessed from inherited classes. 
        So all functions in myoListener is accessible from class main.

   ***Interaction Diagram***

![Interaction Diagram.PNG](https://bitbucket.org/repo/6kjaBb/images/1311692075-Interaction%20Diagram.PNG)

   ***Design Considerations***

    -  There is not any particular pattern to the class design, but if it needs to be described, 
       then it will be like branch pattern. Other classes inherited from top class MYO_DeviceListener 
       that is actually a bridge between Myo armband and camera features. 

   ***User Interface Design***

    - 	Most of the user interfaces (UIs) will look similar to most of the built-in Android camera application,
        but it does not have any camera buttons. It has two buttons one for connecting Myo Armband and other button is for
        giving description about Myo Armband and this application. Connecting Myo Armband button gives little tutorial about
        how to control this application. This application shows Toast message with gesture and performed action when user 
        performs gesture.
![Screenshot_2016-04-17-22-08-45.jpg](https://bitbucket.org/repo/6kjaBb/images/1388991243-Screenshot_2016-04-17-22-08-45.jpg)

**Figure 1**. Example of basic UI left is Portrait mode and right is landscape mode

![Screenshot_2016-04-17-22-09-08.jpg](https://bitbucket.org/repo/6kjaBb/images/2280394317-Screenshot_2016-04-17-22-09-08.jpg)

**Figure 2**. When gesture is performed, the program gives what kind of gesture has been performed and related action.

![Screenshot_2016-04-17-22-06-59.jpg](https://bitbucket.org/repo/6kjaBb/images/1728609512-Screenshot_2016-04-17-22-06-59.jpg)

**Figure 3**. When mini_myo icon is clicked it will direct to this page with little tutorial of the application and button to scan and connect Myo Armband.